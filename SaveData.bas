Attribute VB_Name = "SaveData"
Option Base 1

'每个整点的第一秒存储上个小时的平均统计数值，并存入数据库的小时表
'计算前一小时平均值，并将值记录到小时数据库中
Public Function SaveHourData()
   Set mConn = New Connection   '连接数据库
   mConn.ConnectionString = ConnectString
   mConn.CursorLocation = adUseClient
   mConn.Open
   For i = 1 To RealNum
       HourData(i).Value = HourData(i).Value / HourData(i).N
       K = i
       AddToDB (K)
       HourData(i).N = 0
       HourData(i).Value = 0
   Next i
   
   mConn.Close '关闭数据库
   Set mConn = Nothing
End Function


'每天的第一秒存储前一天的小时平均数，并存入数据库的天记录表
Public Function SaveDayData()

End Function

Public Function SaveToDB()
    Set rs = New ADODB.Recordset
    Set cmd = New ADODB.Command
        cmd.ActiveConnection = mConn
        cmd.CommandText = "InsertData"
        cmd.CommandType = adCmdStoredProc
        cmd.Parameters("@inDay") = Trim(Outday)
        cmd.Parameters("@inHour") = Trim(Variable.hour_R)
        cmd.Parameters("@inTag") = Trim(Tagname)
        cmd.Parameters("@inValue") = Value
    Set rs = cmd.Execute
    Set cmd = Nothing
    Set rs = Nothing
End Function

Public Function DelLastData()
   Set mConn = New Connection   '连接数据库
   mConn.ConnectionString = ConnectString
   mConn.CursorLocation = adUseClient
   mConn.Open
        Set rs = New ADODB.Recordset
        Set cmd = New ADODB.Command
        cmd.ActiveConnection = mConn
        cmd.CommandText = "DelOldData"
        cmd.CommandType = adCmdStoredProc
        cmd.Parameters("@inDay") = CStr(Date - 600)
        
    Set rs = cmd.Execute
    Set cmd = Nothing
    Set rs = Nothing
   mConn.Close '关闭数据库
   Set mConn = Nothing
End Function

'向小时数据库添加一条记录
Private Sub AddToDB(R As Integer)
'   Dim Value As String
'   Dim Tagname As String
   Dim hourr As Integer
   Dim dayy As Integer
   Dim monthh As Integer
   Dim yearr As Integer
   
   
   dayy = CInt(Day(Now))
   monthh = CInt(Month(Now))
   yearr = CInt(Year(Now))
   
   hourr = CInt(Hour(Now))
   If 0 = hourr Then
        hourr = 23
        If 1 = dayy Then
            Select Case monthh
                Case 1:  '****年1月1日  的情况
                    yearr = yearr - 1
                    monthh = 12
                    dayy = 31
                Case 2:  '****年2月1日  的情况
                    monthh = 1
                    dayy = 31
                Case 3:  '****年3月1日  的情况
                    monthh = 2
                    If 0 = yearr Mod 4 Then   '闰年的情况  2.29
                       dayy = 29
                    Else   '非闰年 2.28
                       dayy = 28
                    End If
                Case 4, 6, 9, 11 '小月的情况下，前一月为大月
                    monthh = monthh - 1
                    dayy = 31
                Case 5, 7, 10, 12 '大月情况下，前一月为小月，8月除外
                    monthh = monthh - 1
                    dayy = 30
                Case 8
                    monthh = 7
                    dayy = 31
                
            End Select
          Else
           dayy = dayy - 1
        End If
     Else
        hourr = hourr - 1
   End If
   
   Outday = CStr(yearr) + "-" + CStr(monthh) + "-" + CStr(dayy)
   Variable.hour_R = CStr(hourr)
   Value = HourData(R).Value
   Tagname = HourData(R).Tagname
   
   '''''''''''''''''''''''''''
   '将小时数，标签名，标签平均值
   '存入小时数据库
   '数据库操作代码待完善
   '''''''''''''''''''''''''''
   SaveToDB
   
End Sub

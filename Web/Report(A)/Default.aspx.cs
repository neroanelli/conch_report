﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if ("显示2个数据" == DropDownList1.SelectedValue)
        {
            ObjectDataSource1.SelectMethod = "GetSampleProducts";
            ObjectDataSource1.DataBind();
            ReportViewer1.LocalReport.Refresh();
            
            Response.Write(ReportDB_Company.LReportDB.Count.ToString());
        }
        else
        {
            if ("显示3个数据" == DropDownList1.SelectedValue)
            {
                ObjectDataSource1.SelectMethod = "GetNewProducts";
                ObjectDataSource1.DataBind();
                ReportViewer1.LocalReport.Refresh();
                Response.Write(ReportDB_Company.LReportDB.Count.ToString());
            }
            else 
            {
                ObjectDataSource1.SelectMethod = "GetNowTimeData";
                ObjectDataSource1.DataBind();
                ReportViewer1.LocalReport.Refresh();
                Response.Write(ReportDB_Company.LReportDB.Count.ToString());
            }
        }
    }
    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        //Response.Write(Calendar1.SelectedDate.ToString());
        HistoryData.inDayy = Calendar1.SelectedDate;
        ObjectDataSource1.SelectMethod = "GetHistoryData";
        ObjectDataSource1.DataBind();
        ReportViewer1.LocalReport.Refresh();
        Calendar1.Visible = false;
        //Response.Write(ReportDB_Company.LReportDB.Count.ToString());
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        Calendar1.Visible = true;
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>生产统计报表</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
            Height="781px" Style="z-index: 100; left: 22px; position: absolute; top: 61px"
            Width="906px">
            <LocalReport ReportPath="Report.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="ReportDB_Company" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; <span
            style="font-size: 32pt"><strong>清新海螺熟料线A线报表</strong></span> &nbsp;&nbsp;
        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"
            Style="z-index: 101; left: 421px; position: absolute; top: 6px" Width="213px" Visible="False">
            <asp:ListItem>显示2个数据</asp:ListItem>
            <asp:ListItem>显示3个数据</asp:ListItem>
            <asp:ListItem>数据库</asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="TextBox1" runat="server" Style="z-index: 102; left: 39px; position: absolute;
            top: 817px" Visible="False"></asp:TextBox>
        <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#999999"
            CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt"
            ForeColor="Black" Height="180px" OnSelectionChanged="Calendar1_SelectionChanged"
            Style="z-index: 103; left: 675px; position: absolute; top: 123px" Width="200px" Visible="False">
            <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
            <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
            <SelectorStyle BackColor="#CCCCCC" />
            <WeekendDayStyle BackColor="#FFFFCC" />
            <OtherMonthDayStyle ForeColor="Gray" />
            <NextPrevStyle VerticalAlign="Bottom" />
            <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
            <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
        </asp:Calendar>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetNewProducts"
            TypeName="ReportDB_Company"></asp:ObjectDataSource>
        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/pixelicious_017.png"
            OnClick="ImageButton1_Click" Style="z-index: 105; left: 893px; position: absolute;
            top: 33px" />
    
    </div>
    </form>
</body>
</html>

﻿<%@ Application Language="C#" %>

<script runat="server">

    public static System.Timers.Timer T_Timer = new System.Timers.Timer();
    public static bool Con;
    void Application_Start(object sender, EventArgs e) 
    {
        // 在应用程序启动时运行的代码
        T_Timer.Interval = 20000;
        T_Timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
        T_Timer.Enabled = true;
        Application.Add("Con",true );//控制每小时第5分钟读一次数据库的新纪录到报表数组里
        Data.InitReportData();

    }

    void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
    {
        //ds = Data.GetRealData();
        //string RealConnectionSting = ConfigurationSettings.AppSettings["RealDataConnectionString"];//从web.config中读出数据库连接字符串
        //RealConnectionSting = "DSN=FIX Dynamics Real Time Data";
        //OdbcConnection RealConnection;//实时数据库连接
        //RealConnection = new OdbcConnection(RealConnectionSting);
        //OdbcDataAdapter adapter = new OdbcDataAdapter("select A_CV from FIX", RealConnection);
        //RealConnection.Open();// 11.19日修改
        //adapter.Fill(ds, "FIX");
        //RealConnection.Close();// 11.19日修改
        //adapter.Dispose();
        //Application["ds"]=Data.GetRealData();
        //Data.GetRealData();
        if (DateTime.Now.Minute.Equals(5)) //每个小时的第5分钟执行，可能会执行2次
        {
            if (Application["Con"].Equals(true))//当控制变量为True的情况下，可进行数据操作
            {
                Application.Set("Con", false);
                //更新当天报表数组
                Data.GetNewestData();
            }
            else  //在每小时第5分钟的时间段内，但已经操作过数据库，所以不再操作
            {
                Application.Set("Con", false);
            }

        }
        else//系统时间不在没小时第5分钟内
        {
            Application.Set("Con", true);//将控制变量设置为True，下次进入时间区间，可操作数据库
             
        }
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  在应用程序关闭时运行的代码

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // 在出现未处理的错误时运行的代码

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // 在新会话启动时运行的代码

    }

    void Session_End(object sender, EventArgs e) 
    {
        // 在会话结束时运行的代码。 
        // 注意: 只有在 Web.config 文件中的 sessionstate 模式设置为
        // InProc 时，才会引发 Session_End 事件。如果会话模式设置为 StateServer 
        // 或 SQLServer，则不会引发该事件。

    }
       
</script>

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Data 的摘要说明
/// </summary>
public class Data
{
    public static string[] Tag1 = new string[24]{ "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", };//该标签从1到24点的统计值
    public static string[] Tag2 = new string[24]{ "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", };//该标签从1到24点的统计值
    public static string[] Tag3 = new string[24]{ "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", };//该标签从1到24点的统计值
    public static string[] Tag4 = new string[24] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", };//该标签从1到24点的统计值
    public static string[] Tag5 = new string[24] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", };//该标签从1到24点的统计值
    private static int Renewed = 0;
    

    public   Data()
    {
        //
        // TODO: 在此处添加构造函数逻辑
        //
    }

    /// <summary>
    /// 该方法用在当天到报表中
    /// 被Global.asax中调用，每个小时开始时刻更新标签数组为截止上个小时的最新值
    /// 遇到跨天到情况，则清除标签数组后更新
    /// </summary>
    public  static void GetNewestData()
    {
        if (DateTime.Now.Hour.Equals(Renewed) )
        {
            return;//如果全局变量表示该小说数据已经更新过，则跳出函数。
        }
        string[] tags=new string[5];
        tags[0] = "TIA1"; tags[1] = "TIA2"; tags[2] = "TIA3"; tags[3] = "TIA4"; tags[4] = "TIA5";
      #region 判断是0点还是1点，0点则是报表的最后一行，1点则清空数组后写入数组
        if (0 == DateTime.Now.Hour)//0点的情况，Day需要向前推一天,0点推到23点
        {
            for (int i = 0; i < 5; i++)//每次取一个标签值
            {
                SqlParameter[] myparm = new SqlParameter[3];
                myparm[0].ParameterName = "@inDay";
                myparm[0].SqlDbType = SqlDbType.Char;
                myparm[0].Size = 10;
                myparm[0].Value = GetLastDay();//向前推一天

                myparm[1].ParameterName = "@inHour";
                myparm[1].SqlDbType = SqlDbType.Char;
                myparm[1].Size = 2;
                myparm[1].Value = "23"; //向前推一个小时

                myparm[2].ParameterName = "@inTag";
                myparm[2].SqlDbType = SqlDbType.Char;
                myparm[2].Size = 25;
                myparm[2].Value = tags[i];

                string sqlexec = "FindHourValue";
                //using (SqlConnection conn = new SqlConnection(SqlHelper.ConnectionStringLocalTransaction))
                //{
                //    //打开连接
                //    conn.Open();
                    //调用执行方法
                    //SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlexec, myparm);
                    //Response.Write("<font color=red>操作完成！请检查数据库！</font>");
                    SqlDataReader mydr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringLocalTransaction,CommandType.StoredProcedure,sqlexec,myparm);
                    while (mydr.Read())
                    {
                        
                        switch (i)
                        {
                            case 0:
                                Tag1[23] = mydr["Value"].ToString();
                                break;
                            case 1:
                                Tag2[23] = mydr["Value"].ToString();
                                break;
                            case 2:
                                Tag3[23] = mydr["Value"].ToString();
                                break;
                            case 3:
                                Tag4[23] = mydr["Value"].ToString();
                                break;
                            case 4:
                                Tag5[23] = mydr["Value"].ToString();
                                break;
                            default:
                                break;

                        }
                    }
                    // 关闭datareader
                    mydr.Close();

                //}
            }
        }
        else //非0点的情况，Day不变，小时往前推一个小时
        {
            if (1 == DateTime.Now.Hour)//如果是1点，则清空报表数据数组
            {
                for (int j = 0; j < 24; j++)
                {
                    Tag1[j] = "0";
                    Tag2[j] = "0";
                    Tag3[j] = "0";
                    Tag4[j] = "0";
                    Tag5[j] = "0";
                }
            }
            else
            { 

            for (int i = 0; i < 5; i++)//每次取一个标签值
            {
                SqlParameter[] myparm = new SqlParameter[3];
                myparm[0] = new SqlParameter("@inDay", SqlDbType.Char, 10);
                //myparm[0].ParameterName = "@inDay";
                //myparm[0].SqlDbType = SqlDbType.Char;
                //myparm[0].Size = 10;
                myparm[0].Value = GetDay();

                myparm[1] = new SqlParameter("@inHour", SqlDbType.Char, 2);
                //myparm[1].ParameterName = "inHour";
                //myparm[1].SqlDbType = SqlDbType.Char;
                //myparm[1].Size = 2;
                myparm[1].Value = Convert.ToString(Convert.ToInt32(DateTime.Now.Hour) - 1);//向前推一个小时

                myparm[2] = new SqlParameter("@inTag", SqlDbType.Char, 25);
                //myparm[2].ParameterName = "inTag";
                //myparm[2].SqlDbType = SqlDbType.Char;
                //myparm[2].Size = 25;
                myparm[2].Value = tags[i];

                string sqlexec = "FindHourValue";
                //using (SqlConnection conn = new SqlConnection(SqlHelper.ConnectionStringLocalTransaction))
                //{
                //    //打开连接
                //    conn.Open();
                //调用执行方法
                //SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlexec, myparm);
                //Response.Write("<font color=red>操作完成！请检查数据库！</font>");
                SqlDataReader mydr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, sqlexec, myparm);
                while (mydr.Read())
                {

                    switch (i)
                    {
                        case 0:
                            Tag1[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                            break;
                        case 1:
                            Tag2[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                            break;
                        case 2:
                            Tag3[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                            break;
                        case 3:
                            Tag4[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                            break;
                        case 4:
                            Tag5[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                            break;
                        default:
                            break;

                    }
                }
                // 关闭datareader
                mydr.Close();

                //}
            }
        }
 
        }
      #endregion 

        Renewed = DateTime.Today.Hour;
 
    }

    /// <summary>
    /// 被Global.asax文件的Application_Start调用
    /// 当程序初始运行的时候，初始化当天的报表数据
    /// </summary>
    public static void InitReportData()
    {
        string[] tags = new string[5];
        tags[0] = "TIA1"; tags[1] = "TIA2"; tags[2] = "TIA3"; tags[3] = "TIA4"; tags[4] = "TIA5";
        #region 判断是0点还是1点，0点则是报表的最后一行，1点则清空数组后写入数组
        if (0 == DateTime.Now.Hour)//0点的情况，Day需要向前推一天,0点推到23点
        {
            for (int i = 0; i < 5; i++)//每次取一个标签值
            {
                SqlParameter[] myparm = new SqlParameter[3];
                myparm[0].ParameterName = "@inDay";
                myparm[0].SqlDbType = SqlDbType.Char;
                myparm[0].Size = 10;
                myparm[0].Value = GetLastDay();//向前推一天

                myparm[1].ParameterName = "@inHour";
                myparm[1].SqlDbType = SqlDbType.Char;
                myparm[1].Size = 2;
                myparm[1].Value = "23"; //向前推一个小时

                myparm[2].ParameterName = "@inTag";
                myparm[2].SqlDbType = SqlDbType.Char;
                myparm[2].Size = 25;
                myparm[2].Value = tags[i];

                string sqlexec = "FindHourValue";
                //using (SqlConnection conn = new SqlConnection(SqlHelper.ConnectionStringLocalTransaction))
                //{
                //    //打开连接
                //    conn.Open();
                //调用执行方法
                //SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlexec, myparm);
                //Response.Write("<font color=red>操作完成！请检查数据库！</font>");
                SqlDataReader mydr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, sqlexec, myparm);
                while (mydr.Read())
                {

                    switch (i)
                    {
                        case 0:
                            Tag1[23] = mydr["Value"].ToString();
                            break;
                        case 1:
                            Tag2[23] = mydr["Value"].ToString();
                            break;
                        case 2:
                            Tag3[23] = mydr["Value"].ToString();
                            break;
                        case 3:
                            Tag4[23] = mydr["Value"].ToString();
                            break;
                        case 4:
                            Tag5[23] = mydr["Value"].ToString();
                            break;
                        default:
                            break;

                    }
                }
                // 关闭datareader
                mydr.Close();

                //}
            }
        }
        else //非0点的情况，Day不变，小时往前推一个小时
        {
            if (1 == DateTime.Now.Hour)//如果是1点，则清空报表数据数组
            {
                for (int j = 0; j < 24; j++)
                {
                    Tag1[j] = "0";
                    Tag2[j] = "0";
                    Tag3[j] = "0";
                    Tag4[j] = "0";
                    Tag5[j] = "0";
                }
            }
            else
            {
                for (int k = 0; k < Convert.ToInt32(DateTime.Now.Hour); k++)
                {
                    for (int i = 0; i < 5; i++)//每次取一个标签值
                    {
                        SqlParameter[] myparm = new SqlParameter[3];
                        myparm[0] = new SqlParameter("@inDay", SqlDbType.Char, 10);
                        //myparm[0].ParameterName = "@inDay";
                        //myparm[0].SqlDbType = SqlDbType.Char;
                        //myparm[0].Size = 10;
                        myparm[0].Value = GetDay();

                        myparm[1] = new SqlParameter("@inHour", SqlDbType.Char, 2);
                        //myparm[1].ParameterName = "inHour";
                        //myparm[1].SqlDbType = SqlDbType.Char;
                        //myparm[1].Size = 2;
                        myparm[1].Value = k.ToString();//Convert.ToString(Convert.ToInt32(DateTime.Now.Hour) - 1);//向前推一个小时

                        myparm[2] = new SqlParameter("@inTag", SqlDbType.Char, 25);
                        //myparm[2].ParameterName = "inTag";
                        //myparm[2].SqlDbType = SqlDbType.Char;
                        //myparm[2].Size = 25;
                        myparm[2].Value = tags[i];

                        string sqlexec = "FindHourValue";
                        //using (SqlConnection conn = new SqlConnection(SqlHelper.ConnectionStringLocalTransaction))
                        //{
                        //    //打开连接
                        //    conn.Open();
                        //调用执行方法
                        //SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlexec, myparm);
                        //Response.Write("<font color=red>操作完成！请检查数据库！</font>");
                        SqlDataReader mydr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, sqlexec, myparm);
                        while (mydr.Read())
                        {

                            switch (i)
                            {
                                case 0:
                                    //Tag1[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag1[k] = mydr["Value"].ToString();
                                    break;
                                case 1:
                                    //Tag2[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag2[k] = mydr["Value"].ToString();
                                    break;
                                case 2:
                                    //Tag3[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag3[k] = mydr["Value"].ToString();
                                    break;
                                case 3:
                                    //Tag4[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag4[k] = mydr["Value"].ToString();
                                    break;
                                case 4:
                                    //Tag5[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag5[k] = mydr["Value"].ToString();
                                    break;
                                default:
                                    break;

                            }
                        }
                        // 关闭datareader
                        mydr.Close();

                        //}
                    }
                }
            }

        }
        #endregion 
    }
    
    /// <summary>
    /// 该函数用来获取前一天的日期
    /// </summary>
    /// <returns></returns>
    private static string GetLastDay()
    {
        
        return DateTime.Today.AddDays(-1).Year.ToString() + "-" + DateTime.Today.AddDays(-1).Month.ToString() + "-" + DateTime.Today.AddDays(-1).Day.ToString(); ;

    }

    private static string GetDay()
    {

        return DateTime.Today.Year.ToString() + "-" + DateTime.Today.Month.ToString() + "-" + DateTime.Today.Day.ToString(); ;

    }

}



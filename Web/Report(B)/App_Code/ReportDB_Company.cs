﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Collections;

/// <summary>
/// ReportDB_Company 的摘要说明
/// </summary>
public  class ReportDB_Company
{
    private string _num;
    private string  _name;
    private string  _scole;
    private string _tia1;
    private string _tia2;
    private string _tia3;
    private string _tia4;
    private string _tia5;

    public static  IList<ReportDB_Company> LReportDB = new List<ReportDB_Company>(27);

    public string Num
    {
        get { return _num; }
        set { _num = value; }
    }

    public  string Name 
    {
        get { return _name; }
        set { _name = value; }
    }

    public  string Scole
    {
        get { return _scole; }
        set { _scole = value; }
    }

    public string TIA1
    {
        get { return _tia1; }
        set { _tia1 = value; }
    }

    public string TIA2
    {
        get { return _tia2; }
        set { _tia2 = value; }
    }

    public string TIA3
    {
        get { return _tia3; }
        set { _tia3 = value; }
    }

    public string TIA4
    {
        get { return _tia4; }
        set { _tia4 = value; }
    }

    public string TIA5
    {
        get { return _tia5; }
        set { _tia5 = value; }
    }


    public IList<ReportDB_Company> GetSampleProducts()
    {
        //IList<ReportDB> mySampleProducts = new List<ReportDB>(2);
        LReportDB.Clear();
        ReportDB_Company myReport = new ReportDB_Company();
        myReport.Name = "奶酪";
        myReport.Scole = "$1.39";
        //mySampleProducts.Add(myReport);
        LReportDB.Add(myReport);
        //myReport.Distroy();

        myReport = new ReportDB_Company();
        myReport.Name = "葡萄";
        myReport.Scole = "$0.39";
        //mySampleProducts.Add(myReport);
        LReportDB.Add(myReport);

        //LReportDB[2].Name = "dsa";
        //LReportDB[2].Scole = "23";

        //return (mySampleProducts);
        return (LReportDB);

    }


    public IList<ReportDB_Company> GetNewProducts()
    {
        IList<ReportDB_Company> mySampleProducts = new List<ReportDB_Company>(3);
        
        ReportDB_Company myReport = new ReportDB_Company();
        myReport.Name = "鸡蛋";
        myReport.Scole = "$1.39";
        mySampleProducts.Add(myReport);
        //mySampleProducts[0].Name = "dada";
        //mySampleProducts[0].Scole = "32532";

        myReport = new ReportDB_Company();
        myReport.Name = "牛奶";
        myReport.Scole= "$0.39";
        mySampleProducts.Add(myReport);

        myReport = new ReportDB_Company();
        myReport.Name = "豆浆";
        myReport.Scole = "$5.39";
        mySampleProducts.Add(myReport);
        //LReportDB.Clear();
        return (mySampleProducts);

    }

    public IList<ReportDB_Company> GetNowTimeData()  //2009.6.30完成  ----2009.6.30已完成
    {
        //将Data类中的数据填入报表数据中
        //ReportDB_Company myReprot = new ReportDB_Company();
        //myReprot.Name = Convert.ToString(Convert.ToInt32(DateTime.Now.Hour) - 1);
        //myReprot.Scole = Data.Tag1[Convert.ToInt32(DateTime.Now.Hour) - 1];
        //LReportDB[Convert.ToInt32(DateTime.Now.Hour) - 1] = myReprot;
        
        //LReportDB[Convert.ToInt32(DateTime.Now.Hour) - 1].Name = Data.Tag1[ Convert.ToInt32(DateTime.Now.Hour) - 1];
        //LReportDB[Convert.ToInt32(DateTime.Now.Hour) - 1].Scole = Data.Tag2[Convert.ToInt32(DateTime.Now.Hour) - 1];
        //return (LReportDB);
        Data.GetNewestData();
        LReportDB.Clear();
        ReportDB_Company myReport ;
        for (int j = 0; j < 24; j++)
        {
            myReport = new ReportDB_Company();
            myReport.Num = j.ToString()+"点钟";
            myReport.Name = Data.Tag1[j];
            myReport.Scole =  Data.Tag2[j];
            myReport.TIA1=Data.Tag1[j];
            myReport.TIA2=Data.Tag2[j];
            myReport.TIA3=Data.Tag3[j];
            myReport.TIA4=Data.Tag4[j];
            myReport.TIA5=Data.Tag5[j];
            LReportDB.Add(myReport);
        }
        return (LReportDB);
    }

    /// <summary>
    /// 调用HistoryData中GetHistoryData(string inDay)方法，
    /// 然后将HistoryData类中的数据填入报表数据中
    /// </summary>
    /// <returns></returns>
    public IList<ReportDB_Company> GetHistoryData()//2009.7.1前完成--于2009.6.30完成
    {
        HistoryData.GetHistoryData();
        LReportDB.Clear();
        ReportDB_Company myReport;
        for (int j = 0; j < 8; j++)
        {
            myReport = new ReportDB_Company();
            myReport.Num = j.ToString() + "点钟";
            myReport.Name = HistoryData.Tag1[j];
            myReport.Scole = HistoryData.Tag2[j];
            myReport.TIA1 = HistoryData.Tag1[j];
            myReport.TIA2 = HistoryData.Tag2[j];
            myReport.TIA3 = HistoryData.Tag3[j];
            myReport.TIA4 = HistoryData.Tag4[j];
            myReport.TIA5 = HistoryData.Tag5[j];
            LReportDB.Add(myReport);
        }

        myReport = new ReportDB_Company();
        myReport.Num = "早班平均";
        myReport.Name = HistoryData.Tag1[8];
        myReport.Scole = HistoryData.Tag2[8];
        myReport.TIA1 = HistoryData.Tag1[8];
        myReport.TIA2 = HistoryData.Tag2[8];
        myReport.TIA3 = HistoryData.Tag3[8];
        myReport.TIA4 = HistoryData.Tag4[8];
        myReport.TIA5 = HistoryData.Tag5[8];
        LReportDB.Add(myReport);

        for (int j = 9; j < 17; j++)
        {
            myReport = new ReportDB_Company();
            myReport.Num = Convert.ToString(j-1) + "点钟";
            myReport.Name = HistoryData.Tag1[j];
            myReport.Scole = HistoryData.Tag2[j];
            myReport.TIA1 = HistoryData.Tag1[j];
            myReport.TIA2 = HistoryData.Tag2[j];
            myReport.TIA3 = HistoryData.Tag3[j];
            myReport.TIA4 = HistoryData.Tag4[j];
            myReport.TIA5 = HistoryData.Tag5[j];
            LReportDB.Add(myReport);
        }

        myReport = new ReportDB_Company();
        myReport.Num = "中班平均";
        myReport.Name = HistoryData.Tag1[17];
        myReport.Scole = HistoryData.Tag2[17];
        myReport.TIA1 = HistoryData.Tag1[17];
        myReport.TIA2 = HistoryData.Tag2[17];
        myReport.TIA3 = HistoryData.Tag3[17];
        myReport.TIA4 = HistoryData.Tag4[17];
        myReport.TIA5 = HistoryData.Tag5[17];
        LReportDB.Add(myReport);

        for (int j = 18; j < 26; j++)
        {
            myReport = new ReportDB_Company();
            myReport.Num = Convert.ToString(j - 2) + "点钟";
            myReport.Name = HistoryData.Tag1[j];
            myReport.Scole = HistoryData.Tag2[j];
            myReport.TIA1 = HistoryData.Tag1[j];
            myReport.TIA2 = HistoryData.Tag2[j];
            myReport.TIA3 = HistoryData.Tag3[j];
            myReport.TIA4 = HistoryData.Tag4[j];
            myReport.TIA5 = HistoryData.Tag5[j];
            LReportDB.Add(myReport);
        }

        myReport = new ReportDB_Company();
        myReport.Num = "晚班平均";
        myReport.Name = HistoryData.Tag1[26];
        myReport.Scole = HistoryData.Tag2[26];
        myReport.TIA1 = HistoryData.Tag1[26];
        myReport.TIA2 = HistoryData.Tag2[26];
        myReport.TIA3 = HistoryData.Tag3[26];
        myReport.TIA4 = HistoryData.Tag4[26];
        myReport.TIA5 = HistoryData.Tag5[26];
        LReportDB.Add(myReport);
        return (LReportDB);
    }

    public ReportDB_Company()
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
	}
}

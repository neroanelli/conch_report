﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Data.SqlClient;


/// <summary>
/// HistoryData 的摘要说明
/// </summary>
public class HistoryData
{
    public static string[] Tag1 = new string[27] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", };//该标签从1到24点的统计值
    public static string[] Tag2 = new string[27] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", };//该标签从1到24点的统计值
    public static string[] Tag3 = new string[27] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", };//该标签从1到24点的统计值
    public static string[] Tag4 = new string[27] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", };//该标签从1到24点的统计值
    public static string[] Tag5 = new string[27] { "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", };//该标签从1到24点的统计值
    public static DateTime inDayy;
	public HistoryData()
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
	}

    /// <summary>
    /// 该方法用在历史报表中
    /// 被页面请求通过ReportDB_Company的GetHistoryData(string InDay)方法调用，
    /// 查询数据库后，填充TAGX数组
    /// </summary>
    public static void GetHistoryData()
    {
        string[] tags = new string[5];
        tags[0] = "FI2423C"; tags[1] = "SUMFIRAWB"; tags[2] = "LIA2401C"; tags[3] = "TIA2518C"; tags[4] = "PIA2516C";
        #region 判断是0点还是1点，0点则是报表的最后一行，1点则清空数组后写入数组
        
                for (int j = 0; j < 27; j++)
                {
                    Tag1[j] = "0";
                    Tag2[j] = "0";
                    Tag3[j] = "0";
                    Tag4[j] = "0";
                    Tag5[j] = "0";
                }
          
            
                for (int k = 0; k < 8; k++)
                {
                    for (int i = 0; i < 5; i++)//每次取一个标签值
                    {
                        SqlParameter[] myparm = new SqlParameter[3];
                        myparm[0] = new SqlParameter("@inDay", SqlDbType.Char, 10);
                        
                        myparm[0].Value = GetDay(inDayy);

                        myparm[1] = new SqlParameter("@inHour", SqlDbType.Char, 2);
                        
                        myparm[1].Value = k.ToString();//Convert.ToString(Convert.ToInt32(DateTime.Now.Hour) - 1);//向前推一个小时

                        myparm[2] = new SqlParameter("@inTag", SqlDbType.Char, 25);
                        
                        myparm[2].Value = tags[i];

                        string sqlexec = "FindHourValue";
                        
                        SqlDataReader mydr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, sqlexec, myparm);
                        while (mydr.Read())
                        {

                            switch (i)
                            {
                                case 0:
                                    //Tag1[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag1[k] = mydr["Value"].ToString();
                                    break;
                                case 1:
                                    //Tag2[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag2[k] = mydr["Value"].ToString();
                                    break;
                                case 2:
                                    //Tag3[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag3[k] = mydr["Value"].ToString();
                                    break;
                                case 3:
                                    //Tag4[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag4[k] = mydr["Value"].ToString();
                                    break;
                                case 4:
                                    //Tag5[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag5[k] = mydr["Value"].ToString();
                                    break;
                                default:
                                    break;

                            }
                        }
                        // 关闭datareader
                        mydr.Close();

                        //}
                    }
                }

                Tag1[8]=Convert.ToString((Convert.ToDouble(Tag1[0])+Convert.ToDouble(Tag1[1])+Convert.ToDouble(Tag1[2])+Convert.ToDouble(Tag1[3])+Convert.ToDouble(Tag1[4])+Convert.ToDouble(Tag1[5])+Convert.ToDouble(Tag1[6])+Convert.ToDouble(Tag1[7]))/8);
                Tag2[8] = Convert.ToString((Convert.ToDouble(Tag2[0]) + Convert.ToDouble(Tag2[1]) + Convert.ToDouble(Tag2[2]) + Convert.ToDouble(Tag2[3]) + Convert.ToDouble(Tag2[4]) + Convert.ToDouble(Tag2[5]) + Convert.ToDouble(Tag2[6]) + Convert.ToDouble(Tag2[7])) / 8);
                Tag3[8] = Convert.ToString((Convert.ToDouble(Tag3[0]) + Convert.ToDouble(Tag3[1]) + Convert.ToDouble(Tag3[2]) + Convert.ToDouble(Tag3[3]) + Convert.ToDouble(Tag3[4]) + Convert.ToDouble(Tag3[5]) + Convert.ToDouble(Tag3[6]) + Convert.ToDouble(Tag3[7])) / 8);
                Tag4[8] = Convert.ToString((Convert.ToDouble(Tag4[0]) + Convert.ToDouble(Tag4[1]) + Convert.ToDouble(Tag4[2]) + Convert.ToDouble(Tag4[3]) + Convert.ToDouble(Tag4[4]) + Convert.ToDouble(Tag4[5]) + Convert.ToDouble(Tag4[6]) + Convert.ToDouble(Tag4[7])) / 8);
                Tag5[8] = Convert.ToString((Convert.ToDouble(Tag5[0]) + Convert.ToDouble(Tag5[1]) + Convert.ToDouble(Tag5[2]) + Convert.ToDouble(Tag5[3]) + Convert.ToDouble(Tag5[4]) + Convert.ToDouble(Tag5[5]) + Convert.ToDouble(Tag5[6]) + Convert.ToDouble(Tag5[7])) / 8);
                for (int k = 8; k < 16; k++)
                {
                    for (int i = 0; i < 5; i++)//每次取一个标签值
                    {
                        SqlParameter[] myparm = new SqlParameter[3];
                        myparm[0] = new SqlParameter("@inDay", SqlDbType.Char, 10);

                        myparm[0].Value = GetDay(inDayy);

                        myparm[1] = new SqlParameter("@inHour", SqlDbType.Char, 2);

                        myparm[1].Value = k.ToString();//Convert.ToString(Convert.ToInt32(DateTime.Now.Hour) - 1);//向前推一个小时

                        myparm[2] = new SqlParameter("@inTag", SqlDbType.Char, 25);

                        myparm[2].Value = tags[i];

                        string sqlexec = "FindHourValue";

                        SqlDataReader mydr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, sqlexec, myparm);
                        while (mydr.Read())
                        {

                            switch (i)
                            {
                                case 0:
                                    //Tag1[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag1[k+1] = mydr["Value"].ToString();
                                    break;
                                case 1:
                                    //Tag2[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag2[k+1] = mydr["Value"].ToString();
                                    break;
                                case 2:
                                    //Tag3[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag3[k+1] = mydr["Value"].ToString();
                                    break;
                                case 3:
                                    //Tag4[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag4[k+1] = mydr["Value"].ToString();
                                    break;
                                case 4:
                                    //Tag5[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag5[k+1] = mydr["Value"].ToString();
                                    break;
                                default:
                                    break;

                            }
                        }
                        // 关闭datareader
                        mydr.Close();

                        //}
                    }
                }
                Tag1[17] = Convert.ToString((Convert.ToDouble(Tag1[9]) + Convert.ToDouble(Tag1[10]) + Convert.ToDouble(Tag1[11]) + Convert.ToDouble(Tag1[12]) + Convert.ToDouble(Tag1[13]) + Convert.ToDouble(Tag1[14]) + Convert.ToDouble(Tag1[15]) + Convert.ToDouble(Tag1[16])) / 8);
                Tag2[17] = Convert.ToString((Convert.ToDouble(Tag2[9]) + Convert.ToDouble(Tag2[10]) + Convert.ToDouble(Tag2[11]) + Convert.ToDouble(Tag2[12]) + Convert.ToDouble(Tag2[13]) + Convert.ToDouble(Tag2[14]) + Convert.ToDouble(Tag2[15]) + Convert.ToDouble(Tag2[16])) / 8);
                Tag3[17] = Convert.ToString((Convert.ToDouble(Tag3[9]) + Convert.ToDouble(Tag3[10]) + Convert.ToDouble(Tag3[11]) + Convert.ToDouble(Tag3[12]) + Convert.ToDouble(Tag3[13]) + Convert.ToDouble(Tag3[14]) + Convert.ToDouble(Tag3[15]) + Convert.ToDouble(Tag3[16])) / 8);
                Tag4[17] = Convert.ToString((Convert.ToDouble(Tag4[9]) + Convert.ToDouble(Tag4[10]) + Convert.ToDouble(Tag4[11]) + Convert.ToDouble(Tag4[12]) + Convert.ToDouble(Tag4[13]) + Convert.ToDouble(Tag4[14]) + Convert.ToDouble(Tag4[15]) + Convert.ToDouble(Tag4[16])) / 8);
                Tag5[17] = Convert.ToString((Convert.ToDouble(Tag5[9]) + Convert.ToDouble(Tag5[10]) + Convert.ToDouble(Tag5[11]) + Convert.ToDouble(Tag5[12]) + Convert.ToDouble(Tag5[13]) + Convert.ToDouble(Tag5[14]) + Convert.ToDouble(Tag5[15]) + Convert.ToDouble(Tag5[16])) / 8);

                for (int k = 16; k < 24; k++)
                {
                    for (int i = 0; i < 5; i++)//每次取一个标签值
                    {
                        SqlParameter[] myparm = new SqlParameter[3];
                        myparm[0] = new SqlParameter("@inDay", SqlDbType.Char, 10);

                        myparm[0].Value = GetDay(inDayy);

                        myparm[1] = new SqlParameter("@inHour", SqlDbType.Char, 2);

                        myparm[1].Value = k.ToString();//Convert.ToString(Convert.ToInt32(DateTime.Now.Hour) - 1);//向前推一个小时

                        myparm[2] = new SqlParameter("@inTag", SqlDbType.Char, 25);

                        myparm[2].Value = tags[i];

                        string sqlexec = "FindHourValue";

                        SqlDataReader mydr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringLocalTransaction, CommandType.StoredProcedure, sqlexec, myparm);
                        while (mydr.Read())
                        {

                            switch (i)
                            {
                                case 0:
                                    //Tag1[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag1[k+2] = mydr["Value"].ToString();
                                    break;
                                case 1:
                                    //Tag2[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag2[k+2] = mydr["Value"].ToString();
                                    break;
                                case 2:
                                    //Tag3[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag3[k+2] = mydr["Value"].ToString();
                                    break;
                                case 3:
                                    //Tag4[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag4[k+2] = mydr["Value"].ToString();
                                    break;
                                case 4:
                                    //Tag5[Convert.ToInt32(DateTime.Now.Hour) - 1] = mydr["Value"].ToString();
                                    Tag5[k+2] = mydr["Value"].ToString();
                                    break;
                                default:
                                    break;

                            }
                        }
                        // 关闭datareader
                        mydr.Close();

                        //}
                    }
                }
                Tag1[26] = Convert.ToString((Convert.ToDouble(Tag1[18]) + Convert.ToDouble(Tag1[19]) + Convert.ToDouble(Tag1[20]) + Convert.ToDouble(Tag1[21]) + Convert.ToDouble(Tag1[22]) + Convert.ToDouble(Tag1[23]) + Convert.ToDouble(Tag1[24]) + Convert.ToDouble(Tag1[25])) / 8);
                Tag2[26] = Convert.ToString((Convert.ToDouble(Tag2[18]) + Convert.ToDouble(Tag2[19]) + Convert.ToDouble(Tag2[20]) + Convert.ToDouble(Tag2[21]) + Convert.ToDouble(Tag2[22]) + Convert.ToDouble(Tag2[23]) + Convert.ToDouble(Tag2[24]) + Convert.ToDouble(Tag2[25])) / 8);
                Tag3[26] = Convert.ToString((Convert.ToDouble(Tag3[18]) + Convert.ToDouble(Tag3[19]) + Convert.ToDouble(Tag3[20]) + Convert.ToDouble(Tag3[21]) + Convert.ToDouble(Tag3[22]) + Convert.ToDouble(Tag3[23]) + Convert.ToDouble(Tag3[24]) + Convert.ToDouble(Tag3[25])) / 8);
                Tag4[26] = Convert.ToString((Convert.ToDouble(Tag4[18]) + Convert.ToDouble(Tag4[19]) + Convert.ToDouble(Tag4[20]) + Convert.ToDouble(Tag4[21]) + Convert.ToDouble(Tag4[22]) + Convert.ToDouble(Tag4[23]) + Convert.ToDouble(Tag4[24]) + Convert.ToDouble(Tag4[25])) / 8);
                Tag5[26] = Convert.ToString((Convert.ToDouble(Tag5[18]) + Convert.ToDouble(Tag5[19]) + Convert.ToDouble(Tag5[20]) + Convert.ToDouble(Tag5[21]) + Convert.ToDouble(Tag5[22]) + Convert.ToDouble(Tag5[23]) + Convert.ToDouble(Tag5[24]) + Convert.ToDouble(Tag5[25])) / 8);
        }
        #endregion
    //}

    ///// <summary>
    ///// 该函数用来获取前一天的日期
    ///// </summary>
    ///// <returns></returns>
    //private static string GetLastDay()
    //{

    //    return DateTime.Today.AddDays(-1).Year.ToString() + "-" + DateTime.Today.AddDays(-1).Month.ToString() + "-" + DateTime.Today.AddDays(-1).Day.ToString(); ;

    //}

    private static string GetDay(DateTime inDayy)
    {

        return inDayy.Year.ToString() + "-" + inDayy.Month.ToString() + "-" + inDayy.Day.ToString(); 

    }
}

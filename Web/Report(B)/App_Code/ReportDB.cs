﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// ReportDB 的摘要说明
/// </summary>
public class ReportDB
{
    private string _productName = string.Empty;
    private string _productPrice = string.Empty;
   
    public static IList<ReportDB> LReportDB=new List<ReportDB>(24);

    public string ProductName
    {
        get { return (_productName); }
        set { _productName = value; }
    }

    public string ProductPrice
    {
        get { return (_productPrice); }
        set { _productPrice = value; }
    }

    public IList<ReportDB> GetSampleProducts()
    {
        //IList<ReportDB> mySampleProducts = new List<ReportDB>(2);

        ReportDB myReport = new ReportDB();
        myReport.ProductName = "奶酪";
        myReport.ProductPrice = "$1.39";
        //mySampleProducts.Add(myReport);
        LReportDB.Add(myReport);
        //myReport.Distroy();

        myReport = new ReportDB();
        myReport.ProductName = "葡萄";
        myReport.ProductPrice = "$0.39";
        //mySampleProducts.Add(myReport);
        LReportDB.Add(myReport);

       
        //return (mySampleProducts);
        return (LReportDB);
 
    }


    public IList<ReportDB> GetNewProducts()
    {
        IList<ReportDB> mySampleProducts = new List<ReportDB>(3);

        ReportDB myReport = new ReportDB();
        myReport.ProductName = "鸡蛋";
        myReport.ProductPrice = "$1.39";
        mySampleProducts.Add(myReport);

        myReport = new ReportDB();
        myReport.ProductName = "牛奶";
        myReport.ProductPrice = "$0.39";
        mySampleProducts.Add(myReport);

        myReport = new ReportDB();
        myReport.ProductName = "豆浆";
        myReport.ProductPrice = "$5.39";
        mySampleProducts.Add(myReport);

        return (mySampleProducts);

    }

    /// <summary>
    /// 获取当天最新数据
    /// 
    /// </summary>
    /// <returns></returns>
    //public IList<ReportDB> GetNewestProducts()
    //{
 
    //}

    

	public ReportDB()
	{
		//
		// TODO: 在此处添加构造函数逻辑
		//
        
	}
}

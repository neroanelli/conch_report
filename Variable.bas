Attribute VB_Name = "Variable"
Option Base 1

'用于存储临时采集数据，5秒钟采集一次，如果满足量程要求，
'则进行累加，计数，满1小时后取平均值存入小时数据库
Type Report_RuntimeData
    Tagname As String       '结构体中标签名
    HH As Double            '结构体中量程上限
    LL As Double            '结构体中量程下限
    N As Integer            '结构体中有效采集累加的计数值
    Value As Double         '对应的采集值
    
End Type

'访问IFIX数据接口
Public WorkspaceApp As Object
Public objDataItem As Object

'存储1个小时累计量的 结构体数组
Public HourData() As Report_RuntimeData

'从配置文件中读取的记录点数
Public RealNum As Integer

Public itx As ListItem

Public Record As Boolean '记录小时平均值的控制量
 
Public ConnectString As String ' "Provider=SQLOLEDB.1;Password=123;Persist Security Info=True;User ID=sa;Initial Catalog=ConchReport"

Public mConn As ADODB.Connection
Public rs As ADODB.Recordset
Public cmd As ADODB.Command
Public param As ADODB.Parameter

Public Outday As String
Public hour_R As String
Public Value As Double
Public Tagname As String

Public K As Integer

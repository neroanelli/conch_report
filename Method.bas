Attribute VB_Name = "Method"
Option Base 1
'写INI文件
Public Function WriteFile_INI(Section As String, key As String, Value As String) As Boolean
Dim OpcFile As CIniFile
On Error GoTo ErrHandle
Set OpcFile = New CIniFile
OpcFile.SpecifyIni (App.Path + "\OPC_CONF.ini")
Dim msso As Boolean
msso = OpcFile.WriteString(Section, key, Value)
WriteFile_INI = msso
Exit Function

ErrHandle:
MsgBox Err.Description + "OPEN File"
End Function


'读取INI文件
Public Function ReadFile_INI(Section As String, key As String) As String
Dim OpcFile As CIniFile
On Error GoTo ErrHandle
Set OpcFile = New CIniFile
OpcFile.SpecifyIni (App.Path + "\OPC_CONF.ini")
Dim msso As String
msso = OpcFile.ReadString(Section, key, 80)
ReadFile_INI = msso
Exit Function

ErrHandle:
MsgBox Err.Description + "OPEN File"
End Function





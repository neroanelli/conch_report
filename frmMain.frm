VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMain 
   Caption         =   "报表采集"
   ClientHeight    =   5865
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   11835
   LinkTopic       =   "Form1"
   ScaleHeight     =   5865
   ScaleWidth      =   11835
   StartUpPosition =   3  '窗口缺省
   Begin VB.Timer Timer_Store 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   960
      Top             =   5040
   End
   Begin VB.Timer Timer_Hour 
      Enabled         =   0   'False
      Interval        =   3000
      Left            =   240
      Top             =   5040
   End
   Begin MSComctlLib.ListView lvListView 
      Height          =   4335
      Left            =   0
      TabIndex        =   2
      Top             =   480
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   7646
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      PictureAlignment=   4
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.Toolbar tbToolBar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11835
      _ExtentX        =   20876
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imlToolbarIcons"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "新建"
            Object.ToolTipText     =   "新建"
            ImageKey        =   "New"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "打开"
            Object.ToolTipText     =   "打开"
            ImageKey        =   "Open"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "保存"
            Object.ToolTipText     =   "保存"
            ImageKey        =   "Save"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "打印"
            Object.ToolTipText     =   "打印"
            ImageKey        =   "Print"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "运行"
            Object.ToolTipText     =   "运行"
            ImageKey        =   "Camera"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "左对齐"
            Object.ToolTipText     =   "左对齐"
            ImageKey        =   "Align Left"
            Style           =   2
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "置中"
            Object.ToolTipText     =   "置中"
            ImageKey        =   "Center"
            Style           =   2
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "右对齐"
            Object.ToolTipText     =   "右对齐"
            ImageKey        =   "Align Right"
            Style           =   2
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar sbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   0
      Top             =   5595
      Width           =   11835
      _ExtentX        =   20876
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15214
            Text            =   "状态"
            TextSave        =   "状态"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            TextSave        =   "2012-3-7"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            TextSave        =   "15:42"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog 
      Left            =   2280
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imlToolbarIcons 
      Left            =   720
      Top             =   720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0000
            Key             =   "New"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0112
            Key             =   "Open"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0224
            Key             =   "Save"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0336
            Key             =   "Print"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0448
            Key             =   "Camera"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":055A
            Key             =   "Align Left"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":066C
            Key             =   "Center"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":077E
            Key             =   "Align Right"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "文件(&F)"
      Begin VB.Menu mnuFileNew 
         Caption         =   "新建(&N)"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "打开(&O)..."
      End
      Begin VB.Menu mnuFileClose 
         Caption         =   "关闭(&C)"
      End
      Begin VB.Menu mnuFileBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "保存(&S)"
      End
      Begin VB.Menu mnuFileSaveAs 
         Caption         =   "另存为(&A)..."
      End
      Begin VB.Menu mnuFileSaveAll 
         Caption         =   "全部保存(&L)"
      End
      Begin VB.Menu mnuFileBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileProperties 
         Caption         =   "属性(&I)"
      End
      Begin VB.Menu mnuFileBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFilePageSetup 
         Caption         =   "页面设置(&U)..."
      End
      Begin VB.Menu mnuFilePrintPreview 
         Caption         =   "打印预览(&V)"
      End
      Begin VB.Menu mnuFilePrint 
         Caption         =   "打印(&P)..."
      End
      Begin VB.Menu mnuFileBar3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSend 
         Caption         =   "发送(&D)..."
      End
      Begin VB.Menu mnuFileBar4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileMRU 
         Caption         =   ""
         Index           =   1
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileMRU 
         Caption         =   ""
         Index           =   2
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileMRU 
         Caption         =   ""
         Index           =   3
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileBar5 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "退出(&X)"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "编辑(&E)"
      Begin VB.Menu mnuEditUndo 
         Caption         =   "撤消(&U)"
      End
      Begin VB.Menu mnuEditBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuEditCut 
         Caption         =   "剪切(&T)"
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEditCopy 
         Caption         =   "复制(&C)"
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEditPaste 
         Caption         =   "粘贴(&P)"
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEditPasteSpecial 
         Caption         =   "选择性粘贴(&S)..."
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "视图(&V)"
      Begin VB.Menu mnuViewToolbar 
         Caption         =   "工具栏(&T)"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewStatusBar 
         Caption         =   "状态栏(&B)"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuViewBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewRefresh 
         Caption         =   "刷新(&R)"
      End
      Begin VB.Menu mnuViewOptions 
         Caption         =   "选项(&O)..."
      End
      Begin VB.Menu mnuViewWebBrowser 
         Caption         =   "Web 浏览器(&W)"
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "工具(&T)"
      Begin VB.Menu mnuToolsOptions 
         Caption         =   "选项(&O)..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "帮助(&H)"
      Begin VB.Menu mnuHelpContents 
         Caption         =   "目录(&C)"
      End
      Begin VB.Menu mnuHelpSearchForHelpOn 
         Caption         =   "搜索帮助主题(&S)..."
      End
      Begin VB.Menu mnuHelpBar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "关于(&A) "
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function OSWinHelp% Lib "user32" Alias "WinHelpA" (ByVal hwnd&, ByVal HelpFile$, ByVal wCommand%, dwData As Any)

'访问配置文件的对象
Dim CSVP As New CSVParse


Private Sub Form_Load()

ConnectString = "Provider=SQLOLEDB.1;Password=123;Persist Security Info=True;User ID=sa;Initial Catalog=ConchReport;Data Source=OPCSERVER"
'    Me.Left = GetSetting(App.Title, "Settings", "MainLeft", 1000)
'    Me.Top = GetSetting(App.Title, "Settings", "MainTop", 1000)
    Me.Width = GetSetting(App.Title, "Settings", "MainWidth", 3500)
    Me.Height = GetSetting(App.Title, "Settings", "MainHeight", 3500)
    
    lvListView.Width = Me.Width - 200
    lvListView.Height = (Me.Height - 1500)
    lvListView.ColumnHeaders.Add 1, , "序号", lvListView.Width / 5
    lvListView.ColumnHeaders.Add 2, , "标签名", lvListView.Width / 5
    lvListView.ColumnHeaders.Add 3, , "标签值", lvListView.Width / 5
    lvListView.ColumnHeaders.Add 4, , "上限值", lvListView.Width / 5
    lvListView.ColumnHeaders.Add 5, , "下限值", lvListView.Width / 5
    
    
    RealNum = CInt(ReadFile_INI("RealNum", "Count"))
    ReDim HourData(1 To RealNum) As Report_RuntimeData
    
'    For i = 1 To RealNum
'       Set itx = lvListView.ListItems.Add(, , CStr(i))
'       itx.SubItems(1) = CStr(i + 1)
'       itx.SubItems(2) = CStr(i + 2)
'    Next i
    CSVP.FileName = App.Path + "\OPC_CONF.csv"
    CSVP.LoadNextLine
    For i = 1 To RealNum
        CSVP.LoadNextLine
        HourData(i).Tagname = CSVP.GetField(2)
        HourData(i).HH = Val(CSVP.GetField(3))
        HourData(i).LL = Val(CSVP.GetField(4))
        HourData(i).N = 0
        HourData(i).Value = 0
        Set itx = lvListView.ListItems.Add(, , CSVP.GetField(1))
        itx.SubItems(1) = HourData(i).Tagname
        itx.SubItems(3) = HourData(i).HH
        itx.SubItems(4) = HourData(i).LL
        
    Next i
    
End Sub

Private Sub Timer_Hour_Timer()
'''''''''''''''''
'先判断时间，如果在每个小时的前1秒
'则不进行记录数据
'每个小时的前2秒用来统计前一小时记录的平均值
'1秒后将记录控制量Record置 True
''''''''''''''''''''
If "0" = CStr(Minute(Now)) And "0" = CStr(Second(Now)) Then
    Exit Sub
End If
Record = True
'    For i = 1 To RealNum
'        Set itx = lvListView.ListItems(i)
'        itx.SubItems(2) = CStr(i) + "_" + CStr(Now())
'    Next i
    Dim Data As Double
  On Error Resume Next
    For i = 1 To RealNum
        Set objDataItem = WorkspaceApp.System.findobject("Fix32.FIX." + HourData(i).Tagname + ".F_CV")
        Data = Format(objDataItem.Value, "0.000")
             If (Data <= HourData(i).HH And Data >= HourData(i).LL) Then
                '''''''数据合理 将数据存储做累计，N+1
                HourData(i).Value = HourData(i).Value + Data
                HourData(i).N = HourData(i).N + 1
                Set itx = lvListView.ListItems(i)
                itx.SubItems(2) = CStr(Data) + "_" + CStr(Now())
             End If
                '数据超出量程，不做任何处理
             
    Next i
End Sub



Private Sub Form_Resize()
    If 1 = Me.WindowState Then Exit Sub
    
    
    lvListView.Width = Me.Width - 200
    lvListView.Height = (Me.Height - 1500)
    lvListView.ColumnHeaders.Clear
    
    lvListView.ColumnHeaders.Add 1, , "序号", lvListView.Width / 5
    lvListView.ColumnHeaders.Add 2, , "标签名", lvListView.Width / 5
    lvListView.ColumnHeaders.Add 3, , "标签值", lvListView.Width / 5
    lvListView.ColumnHeaders.Add 4, , "上限值", lvListView.Width / 5
    lvListView.ColumnHeaders.Add 5, , "下限值", lvListView.Width / 5
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim i As Integer


    'close all sub forms
    For i = Forms.Count - 1 To 1 Step -1
        Unload Forms(i)
    Next
    If Me.WindowState <> vbMinimized Then
        SaveSetting App.Title, "Settings", "MainLeft", Me.Left
        SaveSetting App.Title, "Settings", "MainTop", Me.Top
        SaveSetting App.Title, "Settings", "MainWidth", Me.Width
        SaveSetting App.Title, "Settings", "MainHeight", Me.Height
    End If
End Sub

Private Sub tbToolBar_ButtonClick(ByVal Button As MSComctlLib.Button)
    On Error Resume Next
    Select Case Button.key
        Case "新建"
            '应做:添加 '新建' 按钮代码。
            MsgBox "添加 '新建' 按钮代码。"
        Case "打开"
            mnuFileOpen_Click
        Case "保存"
            mnuFileSave_Click
        Case "打印"
            mnuFilePrint_Click
        Case "运行"
            '应做:添加 '运行' 按钮代码。
            'MsgBox "添加 '运行' 按钮代码。"
            CollectData
            
        Case "左对齐"
            '应做:添加 '左对齐' 按钮代码。
            MsgBox "添加 '左对齐' 按钮代码。"
        Case "置中"
            '应做:添加 '置中' 按钮代码。
            MsgBox "添加 '置中' 按钮代码。"
        Case "右对齐"
            '应做:添加 '右对齐' 按钮代码。
            MsgBox "添加 '右对齐' 按钮代码。"
    End Select
End Sub

Private Sub CollectData()
    tbToolBar.Buttons(5).Value = tbrPressed
    
    Set WorkspaceApp = CreateObject("Workspace.Application")
    Timer_Hour.Enabled = True
    Timer_Store.Enabled = True
    
End Sub

Private Sub mnuHelpAbout_Click()
    MsgBox "版本 " & App.Major & "." & App.Minor & "." & App.Revision
End Sub

Private Sub mnuHelpSearchForHelpOn_Click()
    Dim nRet As Integer


    '如果这个工程没有帮助文件，显示消息给用户
    '可以在“工程属性”对话框中为应用程序设置帮助文件
    If Len(App.HelpFile) = 0 Then
        MsgBox "无法显示帮助目录，该工程没有相关联的帮助。", vbInformation, Me.Caption
    Else

    On Error Resume Next
        nRet = OSWinHelp(Me.hwnd, App.HelpFile, 261, 0)
        If Err Then
            MsgBox Err.Description
        End If
    End If

End Sub

Private Sub mnuHelpContents_Click()
    Dim nRet As Integer


    '如果这个工程没有帮助文件，显示消息给用户
    '可以在“工程属性”对话框中为应用程序设置帮助文件
    If Len(App.HelpFile) = 0 Then
        MsgBox "无法显示帮助目录，该工程没有相关联的帮助。", vbInformation, Me.Caption
    Else
        On Error Resume Next
        nRet = OSWinHelp(Me.hwnd, App.HelpFile, 3, 0)
        If Err Then
            MsgBox Err.Description
        End If
    End If

End Sub


Private Sub mnuToolsOptions_Click()
    '应做:添加 'mnuToolsOptions_Click' 代码。
    MsgBox "添加 'mnuToolsOptions_Click' 代码。"
    DbOperate.Show
End Sub

Private Sub mnuViewWebBrowser_Click()
    '应做:添加 'mnuViewWebBrowser_Click' 代码。
    MsgBox "添加 'mnuViewWebBrowser_Click' 代码。"
End Sub

Private Sub mnuViewOptions_Click()
    '应做:添加 'mnuViewOptions_Click' 代码。
    MsgBox "添加 'mnuViewOptions_Click' 代码。"
End Sub

Private Sub mnuViewRefresh_Click()
    '应做:添加 'mnuViewRefresh_Click' 代码。
    MsgBox "添加 'mnuViewRefresh_Click' 代码。"
End Sub

Private Sub mnuViewStatusBar_Click()
    mnuViewStatusBar.Checked = Not mnuViewStatusBar.Checked
    sbStatusBar.Visible = mnuViewStatusBar.Checked
End Sub

Private Sub mnuViewToolbar_Click()
    mnuViewToolbar.Checked = Not mnuViewToolbar.Checked
    tbToolBar.Visible = mnuViewToolbar.Checked
End Sub

Private Sub mnuEditPasteSpecial_Click()
    '应做:添加 'mnuEditPasteSpecial_Click' 代码。
    MsgBox "添加 'mnuEditPasteSpecial_Click' 代码。"
End Sub

Private Sub mnuEditPaste_Click()
    '应做:添加 'mnuEditPaste_Click' 代码。
    MsgBox "添加 'mnuEditPaste_Click' 代码。"
End Sub

Private Sub mnuEditCopy_Click()
    '应做:添加 'mnuEditCopy_Click' 代码。
    MsgBox "添加 'mnuEditCopy_Click' 代码。"
End Sub

Private Sub mnuEditCut_Click()
    '应做:添加 'mnuEditCut_Click' 代码。
    MsgBox "添加 'mnuEditCut_Click' 代码。"
End Sub

Private Sub mnuEditUndo_Click()
    '应做:添加 'mnuEditUndo_Click' 代码。
    MsgBox "添加 'mnuEditUndo_Click' 代码。"
End Sub

Private Sub mnuFileExit_Click()
    '卸载窗体
    Unload Me

End Sub

Private Sub mnuFileSend_Click()
    '应做:添加 'mnuFileSend_Click' 代码。
    MsgBox "添加 'mnuFileSend_Click' 代码。"
End Sub

Private Sub mnuFilePrint_Click()
    '应做:添加 'mnuFilePrint_Click' 代码。
    MsgBox "添加 'mnuFilePrint_Click' 代码。"
End Sub

Private Sub mnuFilePrintPreview_Click()
    '应做:添加 'mnuFilePrintPreview_Click' 代码。
    MsgBox "添加 'mnuFilePrintPreview_Click' 代码。"
End Sub

Private Sub mnuFilePageSetup_Click()
    On Error Resume Next
    With dlgCommonDialog
        .DialogTitle = "页面设置"
        .CancelError = True
        .ShowPrinter
    End With

End Sub

Private Sub mnuFileProperties_Click()
    '应做:添加 'mnuFileProperties_Click' 代码。
    MsgBox "添加 'mnuFileProperties_Click' 代码。"
End Sub

Private Sub mnuFileSaveAll_Click()
    '应做:添加 'mnuFileSaveAll_Click' 代码。
    MsgBox "添加 'mnuFileSaveAll_Click' 代码。"
End Sub

Private Sub mnuFileSaveAs_Click()
    '应做:添加 'mnuFileSaveAs_Click' 代码。
    MsgBox "添加 'mnuFileSaveAs_Click' 代码。"
End Sub

Private Sub mnuFileSave_Click()
    '应做:添加 'mnuFileSave_Click' 代码。
    MsgBox "添加 'mnuFileSave_Click' 代码。"
End Sub

Private Sub mnuFileClose_Click()
    '应做:添加 'mnuFileClose_Click' 代码。
    MsgBox "添加 'mnuFileClose_Click' 代码。"
End Sub

Private Sub mnuFileOpen_Click()
    Dim sFile As String
    

    With dlgCommonDialog
        .DialogTitle = "打开"
        .CancelError = False
        'ToDo: 设置 common dialog 控件的标志和属性
        .Filter = "所有文件 (*.*)|*.*"
        .ShowOpen
        If Len(.FileName) = 0 Then
            Exit Sub
        End If
        sFile = .FileName
    End With
    lvListView.ListItems.Clear
    CSVP.FileName = sFile
    CSVP.LoadNextLine
    For i = 1 To RealNum
        CSVP.LoadNextLine
        HourData(i).Tagname = CSVP.GetField(2)
        HourData(i).HH = Val(CSVP.GetField(3))
        HourData(i).LL = Val(CSVP.GetField(4))
        HourData(i).N = 0
        HourData(i).Value = 0
        Set itx = lvListView.ListItems.Add(, , CSVP.GetField(1))
        itx.SubItems(1) = HourData(i).Tagname
        itx.SubItems(3) = HourData(i).HH
        itx.SubItems(4) = HourData(i).LL
        
    Next i
    'ToDo: 添加处理打开的文件的代码

End Sub

Private Sub mnuFileNew_Click()
    '应做:添加 'mnuFileNew_Click' 代码。
    MsgBox "添加 'mnuFileNew_Click' 代码。"
End Sub


Private Sub Timer_Store_Timer()
   Dim Time As String
   '记录完成后将记录控制量 Record 值False
   '先判断时间
   On Error Resume Next
   
   If "0" = CStr(Minute(Now)) And "0" = CStr(Second(Now)) Then
        '在可记录时间范围内,再判断是否已经记录过
        If Record = True Then
        '记录控制量为True说明这个小时还没有进行记录，可以进行记录
           SaveHourData
           
           ''''''''''''''''''''
           '判断是否 某天的0小时0分0秒
           '如果是，则调用DelLastData 删除600天前的数据
           '
           '
           '''''''''''''''''''''
           DbOperate.Text1.Text = DbOperate.Text1.Text + "------" + CStr(Now)
        End If
     Record = False
   End If
   
   
End Sub
